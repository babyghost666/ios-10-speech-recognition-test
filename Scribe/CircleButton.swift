//
//  CircleButton.swift
//  Scribe
//
//  Created by Peter Leung on 5/9/2016.
//  Copyright © 2016 winandmac Media. All rights reserved.
//

import UIKit

@IBDesignable

class CircleButton: UIButton {

    @IBInspectable var cornerRadius:CGFloat = 30.0 {
        didSet{
            SetUpView()
        }
        
    }
    
    override func prepareForInterfaceBuilder() {
       SetUpView()
    }
    
    func SetUpView(){
        layer.cornerRadius = cornerRadius
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
