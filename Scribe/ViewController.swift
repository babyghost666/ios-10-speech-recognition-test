//
//  ViewController.swift
//  Scribe
//
//  Created by Peter Leung on 4/9/2016.
//  Copyright © 2016 winandmac Media. All rights reserved.
//

import UIKit
import Speech
import AVFoundation

class ViewController: UIViewController,AVAudioPlayerDelegate {

    @IBOutlet weak var textFieldTrans: UITextView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var AudioPlayer: AVAudioPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        activityIndicator.isHidden = true
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        player.stop()
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }
    
    
    func requestSpeechAuth() {
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            if authStatus == SFSpeechRecognizerAuthorizationStatus.authorized {
                if let path = Bundle.main.url(forResource: "test", withExtension: "m4a"){
                    do {
                        let sound = try AVAudioPlayer(contentsOf: path)
                        self.AudioPlayer = sound
                        self.AudioPlayer.delegate = self
                        sound.play()
                    } catch {
                        
                    }
                    
                    let recogniser = SFSpeechRecognizer()
                    let request = SFSpeechURLRecognitionRequest(url: path)
                    recogniser?.recognitionTask(with: request, resultHandler: { (result, error) in
                        if let error = error {
                            print("Error \(error)")
                        }else {
                            self.textFieldTrans.text = result?.bestTranscription.formattedString
                        }
                        
                    })
                }
                
                
            }
        }
    }
    @IBAction func startTrans(_ sender: AnyObject) {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        requestSpeechAuth()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

